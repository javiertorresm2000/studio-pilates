//Planos Functions

function btn_checked_small(){
    var small=document.getElementById("btn-small");
    var medium=document.getElementById("btn-medium");
    var large=document.getElementById("btn-large");

    if(small.classList.contains("btn-plano")){
        small.classList.remove("btn-plano");
        small.classList.add("btn-checked");

        medium.classList.remove("btn-checked");
        medium.classList.add("btn-plano");

        large.classList.remove("btn-checked");
        large.classList.add("btn-plano");

        if(document.querySelector("#no_fisi").textContent>3){
            document.querySelector("#no_fisi").textContent=1;
            document.getElementById("cant").textContent="10.00";
            document.getElementById("final_price").textContent="10.00";
        }
    }   
}

function btn_checked_medium(){
    var small=document.getElementById("btn-small");
    var medium=document.getElementById("btn-medium");
    var large=document.getElementById("btn-large");

    if(medium.classList.contains("btn-plano")){
        small.classList.remove("btn-checked");
        small.classList.add("btn-plano");

        medium.classList.remove("btn-plano");
        medium.classList.add("btn-checked");

        large.classList.remove("btn-checked");
        large.classList.add("btn-plano");

        if(document.querySelector("#no_fisi").textContent<4 || document.querySelector("#no_fisi").textContent>10){
            document.querySelector("#no_fisi").textContent=4;
            document.getElementById("cant").textContent="40.00";
            document.getElementById("final_price").textContent="40.00";
        }
    }  
}

function btn_checked_large(){
    var small=document.getElementById("btn-small");
    var medium=document.getElementById("btn-medium");
    var large=document.getElementById("btn-large");

    if(large.classList.contains("btn-plano")){
        small.classList.remove("btn-checked");
        small.classList.add("btn-plano");

        medium.classList.remove("btn-checked");
        medium.classList.add("btn-plano");

        large.classList.remove("btn-plano");
        large.classList.add("btn-checked");

        if(document.querySelector("#no_fisi").textContent<11){
            document.querySelector("#no_fisi").textContent=11;
            document.getElementById("cant").textContent="110.00";
            document.getElementById("final_price").textContent="110.00";
        }
    }
}

function btn_sub(){
    var no_fisi=document.querySelector("#no_fisi").textContent;
    if(no_fisi>1){
        var resta=parseFloat(no_fisi)-1;
        var monto=resta*10;
        var montofinal=monto;
        document.getElementById("no_fisi").textContent=resta;
        document.getElementById("cant").textContent=monto+".00";
        document.getElementById("final_price").textContent=montofinal+".00";
        if(resta>=1 && resta<4){
            btn_checked_small();
        }
        else{
            if(resta>=1 && resta<11){
                btn_checked_medium();
            }
            else{
                btn_checked_large();
            }
        }
    }
}

function btn_adi(){
    var no_fisi2=document.querySelector("#no_fisi").textContent;
    if(no_fisi2>=1){
        var suma=parseFloat(no_fisi2)+1;
        var monto=suma*10.00;
        var montofinal=monto;
        document.getElementById("no_fisi").textContent=suma;
        document.getElementById("cant").textContent=monto+".00";
        document.getElementById("final_price").textContent=montofinal+".00";
        if(suma>=1 && suma<4){
            btn_checked_small();
        }
        else{
            if(suma>=1 && suma<11){
                btn_checked_medium();
            }
            else{
                btn_checked_large();
            }
        }
    }
}

function div_checked(){
    var div_check=document.getElementById("div-check-fisio");
    if(div_check.className=="div-checked"){
        div_check.classList.remove("div-checked");
        div_check.classList.add("div-non-checked");
    }else{
        div_check.classList.add("div-checked");
        div_check.classList.remove("div-non-checked");
    }
}

